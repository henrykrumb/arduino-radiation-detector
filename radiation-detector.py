#!/usr/bin/env python3
import datetime
import time

# use
#     pip install click pyserial
# to install dependencies
import click
import serial


@click.command()
@click.option(
    '--port', '-p',
    type=str,
    help='Serial port. Default is /dev/ttyACM0',
    default='/dev/ttyACM0')
@click.option('--baud', '-b', type=int, help='Baud rate', default=9600)
def main(port, baud):
    with serial.Serial(port, baud) as ser:
        time.sleep(2)
        ser.flushInput()
        ser.flushOutput()
        
        # print info string
        print('connected to serial interface')
        b = ser.write(bytes([0x00]))
        print(b)
        lines = int.from_bytes(ser.read(1), 'big')
        print(lines)
        for i in range(lines):
            line = ser.readline().decode('ASCII').strip()
            print(line)

        while True:
            b = ser.write(bytes([0x03]))
            CPM = int.from_bytes(ser.read(4), 'little')
            CF = 0.008120
            mSv = CF * CPM
            print(f'{CPM} CPM => {mSv:.4f} µSv/h')


if __name__ == '__main__':
    main()
