use <shell.scad>;

$fn = 40;

wall = 2;
tol = 0.1;

module screw_hole() {
  cylinder(h=2 + 0.01, r1=5.5 / 2 + tol, r2=3 / 2 + tol);
  translate([0, 0, 2])
  cylinder(h=4, r=3 / 2 + tol);
}

module bottom_shell() {
  difference() {
    union() {
      translate([-wall, -wall, -wall - 0.01])
      linear_extrude(6 + wall)
      offset(r=5)
      square([63 + wall * 2, 107 + wall * 2]);
      
      translate([-wall / 2, -wall / 2, -wall - 0.01])
      linear_extrude(7 + wall)
      offset(r=5)
      square([63 + wall, 107 + wall]);
    }
    
    linear_extrude(7)
    offset(r=5)
    square([63, 107]);
    
    translate([2.5, 2.5, -wall - 0.02])
    screw_hole();
    
    translate([2.5 + 57, 2.5, -wall - 0.02])
    screw_hole();
    
    translate([2.5 + 57, 2.5 + 101.5, -wall - 0.02])
    screw_hole();
    
    translate([2.5, 2.5 + 101.5, -wall - 0.02])
    screw_hole();
  }
}

module top_shell() {
  difference() {
    translate([-wall, -wall, 0])
    linear_extrude(12 + wall + wall / 2)
    offset(r=5)
    square([63 + wall * 2, 107 + wall * 2]);
  
    translate([-wall / 2, -wall / 2, -0.01])
    linear_extrude(wall / 2)
    offset(r=5)
    square([63 + wall, 107 + wall]);
    
    linear_extrude(12 + wall / 2 + 0.1)
    offset(r=5)
    square([63, 107]);
    
    
    translate([2.5, 2.5, wall + 10 + 0.02])
    cylinder(h=4, r=3 / 2 + tol);
    
    translate([2.5 + 57, 2.5, wall + 10 + 0.02])
    cylinder(h=4, r=3 / 2 + tol);
    
    translate([2.5 + 57, 2.5 + 101.5, wall + 10 + 0.02])
    cylinder(h=4, r=3 / 2 + tol);
    
    translate([2.5, 2.5 + 101.5, wall + 10 + 0.02])
    cylinder(h=4, r=3 / 2 + tol);
    
    // pin headers
    translate([2.5 + 20 - 1, 0, 10]) {
      cube([8 + 2, 5, 10]);
      
      translate([0, 0, 4.1])
      linear_extrude(1)
      rotate([0, 0, 90]) {
        translate([6, -2, 0])
        text("GND", font="monospace:style=bold", size=3);
        
        translate([7, -6, 0])
        text("5V", font="monospace:style=bold", size=3);
        
        translate([6, -10, 0])
        text("VIN", font="monospace:style=bold", size=3);
      }
    }
    
    // audio jack
    translate([2.5 + 40, -5 - wall - 0.01, -1e-3])
    cube([9 + tol, wall + tol, 6 + tol]);
    
    // switch
    translate([2.5 + 24 - 1, 107 + 4, -1e-3])
    cube([7 + tol, wall + 1 + tol, 6 + tol]);
    
    // beep
    translate([2.5 + 57 - 7.5, 2.5 + 40, 10])
    cylinder(h=10, r=1.5);
  }
}

translate([0, 0, 20])
top_shell();

bottom_shell();