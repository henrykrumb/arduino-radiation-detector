# arduino-radiation-detector

Arduino firmware to interface with DIY Geiger Counter kits.


## Usage
Connect the pins of your radiation detector module to your Arduino.
By default, digital pin 2 is used for the radiation detector's signal.

Upload firmware to your Arduino UNO board.
You can use the python example (radiation-detector.py) as a reference.

Also check out the "scad/" folder containing a simple enclosure for the
detector board.
