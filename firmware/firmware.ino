#define GEIGER_PIN 2

#define BAUD 9600

#define DEVICE_ID "839491ee-27cb-4857-b1c8-6bfc7fbb692d"
#define BOARD_VERSION "1.0.0"

enum {
  COMMAND_INFO = 0,
  COMMAND_RESET,
  COMMAND_ERROR_CODE,
  COMMAND_MEASURE,
  COMMAND_SENTINEL
};

enum {
  ERR_NONE = 0,
  ERR_COMMAND_NOT_FOUND,
  ERR_SENTINEL
};


uint8_t error_code;
uint32_t event_index;
#define MAX_EVENTS 256
unsigned long events[MAX_EVENTS];
uint32_t CPM;


void info() {
  // number of lines, simplifies parsing on receiving side
  Serial.write(5);
  // ID
  Serial.println(DEVICE_ID);
  // board version
  Serial.println(BOARD_VERSION);
  // add commands here
  Serial.println("COMMAND_INFO COMMAND_RESET COMMAND_ERROR_CODE COMMAND_MEASURE");
  // date & time of compilation
  Serial.println(__DATE__);
  Serial.println(__TIME__);
}


void reset() {
  error_code = 0;
}


void ISR_impulse() {
  events[event_index] = millis();
  event_index = (event_index + 1) % MAX_EVENTS;
}


void setup() {
  error_code = 0;
  memset(events, 0, sizeof(events));
  CPM = 0;
  event_index = 0;
  Serial.begin(BAUD);
  
  pinMode(GEIGER_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(GEIGER_PIN), ISR_impulse, FALLING);
}


void loop() {
  while (Serial.available() == 0) {}

  uint8_t command = Serial.read();

  // split command into command & payload
  uint8_t payload = command >> 4;
  command = command & 0xF;

  uint32_t current_time = millis();
  
  switch (command) {
    case COMMAND_INFO:
    {
      info();
    }
    break;

    case COMMAND_RESET:
    {
      reset();
    }
    break;

    case COMMAND_ERROR_CODE:
    {
      Serial.write(error_code);
      error_code = 0;
    }
    break;

    case COMMAND_MEASURE:
    {
      CPM = 0;
      for (int i = 0; i < MAX_EVENTS; ++i) {
        if (current_time - events[i] >= 60000L) {
          events[i] = 0;
        }
        if (events[i] != 0) {
          CPM++;
        }
      }
      // send as little endian encoded int
      Serial.write(CPM & 0xFF);
      Serial.write((CPM & 0xFF00) >> 8);
      Serial.write((CPM & 0xFF0000) >> 16);
      Serial.write((CPM & 0xFF000000) >> 24);
    }
    break;

    default:
    {
      error_code = ERR_COMMAND_NOT_FOUND;
    }
    break;
  }
}
